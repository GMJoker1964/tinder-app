import React, { Suspense } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import routes from "./rootRoute";
import PrivateRoute from "./HOC/PrivateRouter";

function App() {
  return (
    <Router>
      <Suspense fallback={<div>loading ....</div>}>
        <Switch>
          {routes.map((route) => {
            if (route.public) {
              return (
                <Route
                  key={route}
                  path={route.path}
                  exact={route.exact}
                  component={route.component}
                />
              );
            }
            return <PrivateRoute h="hello" {...route} key={route} />;
          })}
        </Switch>
      </Suspense>
    </Router>
  );
}

export default App;
