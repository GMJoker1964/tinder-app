import React from "react";

function BtnHeart(props) {
  return (
    <div className="box-icon btn-heart">
      <i className="bi bi-heart-fill" />
    </div>
  );
}

export default BtnHeart;
