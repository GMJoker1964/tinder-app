import React from "react";
import "./btn.scss";

function BtnNope(props) {
  return (
    <div className="box-icon btn-nope">
      <i className="bi bi-x" />
    </div>
  );
}

export default BtnNope;
