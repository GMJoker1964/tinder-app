import React from "react";
import "./btn.scss";

function BtnStar(props) {
  return (
    <div className="box-icon">
      <i className="bi bi-star-fill" />
    </div>
  );
}

export default BtnStar;
