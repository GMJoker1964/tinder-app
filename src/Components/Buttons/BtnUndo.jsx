import React from "react";

import "./btn.scss";

function BtnUndo(props) {
  return (
    <div className="btn-undo box-icon">
      <i className="bi bi-arrow-counterclockwise" />
    </div>
  );
}

export default BtnUndo;
