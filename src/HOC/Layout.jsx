import React from "react";
import NavbarDesktop from "../Components/NavbarDesktop";

const Layout = ({ left, right }) => (
  <>
    <div className="row layout">
      <div className="col left">
        <NavbarDesktop />
        {left.map((ComponentLeft) => (
          <React.Fragment key={ComponentLeft}>
            <ComponentLeft />
          </React.Fragment>
        ))}
      </div>

      <div className="col right">
        {right.map((ComponentRight) => (
          <React.Fragment key={ComponentRight}>
            <ComponentRight />
          </React.Fragment>
        ))}
      </div>
    </div>
  </>
);

export default Layout;
