import React from "react";
import axiosClient from "../apis/axiosClient";
import { authConstants } from "../constansts/index";

export const Signin = (tokenId, imgDefault) => async (dispatch) => {
  dispatch({ type: authConstants.LOGIN_REQUEST });

  const res = await axiosClient.post("/auth/signin", {
    tokenId,
  });
  if (res.status !== 400) {
    const { token, user } = res.data;

    localStorage.setItem("token", token);
    localStorage.setItem("imgDefault", imgDefault);
    localStorage.setItem("user", JSON.stringify(user));
    dispatch({
      type: authConstants.LOGIN_SUCCESS,
      payload: {
        token,
        user,
      },
    });
  } else {
    if (res.status === 400) {
      dispatch({
        type: authConstants.LOGIN_FAILURE,
        payload: { error: res.data.error },
      });
    }
  }
};

export const Signout = () => async (dispatch) => {
  dispatch({ type: authConstants.LOGOUT_REQUEST });
  const res = await axiosClient.post("/auth/signout");

  if (res.status === 200) {
    localStorage.clear();
    dispatch({ type: authConstants.LOGOUT_SUCCESS });
  } else {
    dispatch({
      type: authConstants.LOGOUT_FAILURE,
      payload: { error: res.data.error },
    });
  }
};
