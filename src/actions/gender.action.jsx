import axiosClient from "../apis/axiosClient";
import { getGenderConstants } from "../constansts/index";

export const GetGenders = () => async (dispatch) => {
  dispatch({ type: getGenderConstants.GET_GENDERS_REQUEST });

  const res = await axiosClient.get("/gender/getGender");
  if (res.status === 200) {
    dispatch({
      type: getGenderConstants.GET_GENDERS_SUCCESS,
      payload: { genderList: res.data.allGenders },
    });
  } else {
    dispatch({
      type: getGenderConstants.GET_GENDERS_FAILURE,
      payload: { error: res.err },
    });
  }
};
