const action = (type, data) => ({
  type,
  payload: data,
});

export default action;
