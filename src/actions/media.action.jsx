import axiosClient from "../apis/axiosClient";
import { mediaConstants } from "../constansts/index";

export const CreateMedia = (media) => async (dispatch) => {
  dispatch({ type: mediaConstants.CREATE_MEDIA_REQUEST });

  const res = await axiosClient.post("/userPhoto/createPhoto", media);

  if (res.status === 200) {
    dispatch({
      type: mediaConstants.CREATE_MEDIA_SUCCESS,
      payload: {
        msg: "add setting success",
      },
    });
  } else {
    dispatch({
      type: mediaConstants.CREATE_MEDIA_FAILURE,
      payload: {
        msg: "add setting failure",
      },
    });
  }
};
