import axiosClient from "../apis/axiosClient";
import { settingConstants } from "../constansts/index";

export const AddSetting = (userId, genderId) => async (dispatch) => {
  dispatch({ type: settingConstants.ADD_SETTING_REQUEST });

  const res = await axiosClient.post("/setting/addSetting", {
    userId,
    genderId,
  });

  if (res.status === 200) {
    dispatch({
      type: settingConstants.ADD_SETTING_SUCCESS,
      payload: {
        msg: "add setting success",
      },
    });
  } else {
    dispatch({
      type: settingConstants.ADD_SETTING_FAILURE,
      payload: {
        msg: "add setting failure",
      },
    });
  }
};
