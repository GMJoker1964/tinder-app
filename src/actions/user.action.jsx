import axiosClient from "../apis/axiosClient";
import { userConstants } from "../constansts/index";

export const UpdateStatus = (id) => async (dispatch) => {
  dispatch({ type: userConstants.UPDATE_PROFILE_REQUEST });

  const res = await axiosClient.post(`/user/updateStatus?id=${id}`);

  if (res.status === 200) {
    dispatch({
      type: userConstants.UPDATE_PROFILE_SUCCESS,
      msg: "update status success",
    });
  } else {
    dispatch({
      type: userConstants.UPDATE_PROFILE_FAILURE,
    });
  }
};

export const EditProfile = (id, data) => async (dispatch) => {
  dispatch({ type: userConstants.UPDATE_PROFILE_REQUEST });

  const res = await axiosClient.put(`/user/editProfile?id=${id}`, data);

  if (res.status === 200) {
    dispatch({
      type: userConstants.UPDATE_PROFILE_SUCCESS,
      msg: "update profile success",
    });
  } else {
    dispatch({
      type: userConstants.UPDATE_PROFILE_FAILURE,
    });
  }
};
