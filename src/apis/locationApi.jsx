import axiosClient from "./axiosClient";

const locationApi = {};
locationApi.update = (data) => {
  const url = "/location";
  return axiosClient.put(url, data);
};

export default locationApi;
