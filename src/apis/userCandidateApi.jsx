import axiosClient from "./axiosClient";

const userCandidateApi = {};

userCandidateApi.getCandidate = (userId, params = {}) => {
  const url = `/user/${userId}/getRandomUser`;
  return axiosClient.get(url, { params });
};

export default userCandidateApi;
