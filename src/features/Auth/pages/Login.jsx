import React from "react";
import { GoogleLogin } from "react-google-login";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Signin } from "../../../actions/auth.action";

import "../auth.scss";

function Login(props) {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  const responseSuccessGoogle = (res) => {
    dispatch(Signin(res.tokenId, res.profileObj.imageUrl));
  };
  const responseFailureGoogle = (res) => {};
  if (auth.authenticate === true && auth.user.status === 1) {
    history.push("/createAccount");
  }
  if (auth.authenticate === true && auth.user.status === 2) {
    history.push("/main");
  }
  return (
    <main className="main">
      <div className="container">
        <section className="wrapper">
          <div className="heading">
            <h1 className="text text-large">Sign In</h1>
          </div>
          <div className="striped">
            <span className="striped-line" />
            <span className="striped-text">Or</span>
            <span className="striped-line" />
          </div>
          <div className="method">
            <div className="method-control">
              <a href="#" className="method-action">
                <GoogleLogin
                  clientId="554778092924-pdk065lnmfrp33vbv5mukdkike39ptup.apps.googleusercontent.com"
                  buttonText="Login"
                  onSuccess={responseSuccessGoogle}
                  onFailure={responseFailureGoogle}
                  cookiePolicy="single_host_origin"
                />
              </a>
            </div>
          </div>
        </section>
      </div>
    </main>
  );
}

export default Login;
