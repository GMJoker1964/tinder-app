import React from "react";
import EmtyMessage from "../components/EmtyMessage";

function ListMessage({ messages }) {
  const lengthMessages = messages.length - 1;
  const checkLengthMessage = lengthMessages > 0;
  return <>{!checkLengthMessage && <EmtyMessage />}</>;
}

export default ListMessage;
