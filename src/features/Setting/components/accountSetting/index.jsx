import React from "react";
import { Link } from "react-router-dom";
import SettingButton from "../settingButton";
import "./style.scss";

function AccountSetting(props) {
  return (
    <div>
      <Link to="/profile/account">
        <SettingButton
          title="Manage Payment Account"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
      <SettingButton title="Restore Purchase" />
      <Link to="/profile/emailAccount">
        <SettingButton
          title="Email"
          currentValue="abc@gmail.com"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
      <Link to="/profile/phoneAccount">
        <SettingButton
          title="Phone Number"
          currentValue="0345678912"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
      <SettingButton title="Promo Code" />
      <h6>Veritified Phone Number and Email help secure your account.</h6>
    </div>
  );
}

export default AccountSetting;
