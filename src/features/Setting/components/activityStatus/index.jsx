import React from "react";
import { Link } from "react-router-dom";
import SettingButton from "../settingButton";

function ActivityStatus(props) {
  return (
    <div>
      <Link to="/profile/recentlyActive">
        <SettingButton
          title="Recently Active Status"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
    </div>
  );
}

export default ActivityStatus;
