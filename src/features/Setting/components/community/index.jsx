import React from "react";
import SettingButton from "../settingButton";

function Community(props) {
  return (
    <div>
      <SettingButton
        title="Community Guildlines"
        icon={<i className="fas fa-external-link-alt" />}
      />
      <SettingButton
        title={`Safety ${"&"} Policy Center`}
        icon={<i className="fas fa-external-link-alt" />}
      />
      <SettingButton
        title="Safety Tips"
        icon={<i className="fas fa-external-link-alt" />}
      />
    </div>
  );
}

export default Community;
