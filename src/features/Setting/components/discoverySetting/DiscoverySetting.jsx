import React, { useState } from "react";
import { Link } from "react-router-dom";
import SwitchesButton from "../../switchesButton";
import SettingButton from "../settingButton";
import SliderButton from "../sliderButton";
import "./style.scss";

function DiscoverySetting(props) {
  const [km, setKm] = useState(50);
  const [age, setAge] = useState([25, 75]);

  return (
    <div>
      <SettingButton
        title="Location"
        currentValue="Viet Nam"
        icon={<i className="fas fa-angle-right" />}
      />
      <SliderButton value={km} setValue={setKm} title="Maximum Distance" />
      <Link to="/profile/gender">
        <SettingButton
          title="Looking For"
          currentValue="Male"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>

      <SliderButton
        title="Age Range"
        value={age}
        setValue={setAge}
        isRangeAgeButton="true"
      />
      <SwitchesButton title="Global" />
      <Link to="/profile/languages">
        <SettingButton
          title="Preferred Language"
          currentValue="English"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>

      <h6>
        Going global will allow you to see people from around the world after
        you run out of profile nearby.
      </h6>
      <SwitchesButton title="Show me on Tinder" />
      <h6>
        While turn off, you will allow to see people from around the world after
        you have run out of profiles nearby
      </h6>
    </div>
  );
}

export default DiscoverySetting;
