import React from "react";
import SettingButton from "../settingButton";

function HelpSupport(props) {
  return (
    <div>
      <SettingButton
        title={`Help ${"&"} Support`}
        icon={<i className="fas fa-external-link-alt" />}
      />
    </div>
  );
}

export default HelpSupport;
