import React from "react";
import { Link } from "react-router-dom";
import SettingButton from "../settingButton";

function Language(props) {
  return (
    <div>
      <Link to="/profile/language">
        <SettingButton
          title="Language"
          currentValue="English"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
    </div>
  );
}

export default Language;
