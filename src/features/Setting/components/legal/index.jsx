import React from "react";
import SettingButton from "../settingButton";

function Legal(props) {
  return (
    <div>
      <SettingButton title="Privacy Settings" />
      <SettingButton
        title="Cookie Policy"
        icon={<i className="fas fa-external-link-alt" />}
      />
      <SettingButton
        title="Privacy Policy"
        icon={<i className="fas fa-external-link-alt" />}
      />
      <SettingButton
        title="Term of Service"
        icon={<i className="fas fa-external-link-alt" />}
      />
    </div>
  );
}

export default Legal;
