import React from "react";
import "./LogoBox.scss";

function LogoBox(props) {
  const { img, title } = props;
  return (
    <div className="logo-box">
      <div className="box-display">
        <img
          alt="img"
          className="image"
          src={`${process.env.PUBLIC_URL}assets/images/${img}`}
        />
        <span className="title">{title}</span>
      </div>
    </div>
  );
}

export default LogoBox;
