import React from "react";
import SettingButton from "../settingButton";
import "./style.scss";

function Logout(props) {
  return (
    <div className="log-out">
      <SettingButton title="Log Out" />
    </div>
  );
}

export default Logout;
