import React from "react";
import { Link } from "react-router-dom";
import SettingButton from "../settingButton";

function Notification(props) {
  return (
    <div>
      <Link to="/profile/email">
        <SettingButton
          title="Email"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
      <Link to="/profile/push">
        <SettingButton
          title="Push Notification"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
    </div>
  );
}

export default Notification;
