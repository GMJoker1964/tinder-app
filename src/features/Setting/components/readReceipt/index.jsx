import React from "react";
import { Link } from "react-router-dom";
import SettingButton from "../settingButton";

function ReadReceipts(props) {
  return (
    <div>
      <Link to="/profile/readReceipts">
        <SettingButton
          title="Manage Read Receipts"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
    </div>
  );
}

export default ReadReceipts;
