import React from "react";
import "./style.scss";

function SettingButton(props) {
  const { title, icon, currentValue, isBorderBottom } = props;

  return (
    <div
      style={{
        borderBottom: isBorderBottom && "1px solid rgb(180, 178, 178)",
      }}
      className="setting-button"
    >
      <span>{title}</span>
      <div className="right-side">
        {currentValue && <span>{currentValue}</span>}
        {icon && icon}
      </div>
    </div>
  );
}

export default SettingButton;
