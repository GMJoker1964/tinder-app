import React, { useState } from "react";
import ReactSlider from "react-slider";
import "./style.scss";

function SliderButton(props) {
  const { value, setValue, title, isRangeAgeButton } = props;

  return (
    <div className="slider-button">
      <div className="label">
        <span>{title}: </span>
        {isRangeAgeButton ? (
          <div>
            <span>
              {value[0]} - {value[1]}
            </span>
          </div>
        ) : (
          <span>{value}</span>
        )}
      </div>
      <ReactSlider
        className="horizontal-slider"
        value={isRangeAgeButton ? value : null}
        defaultValue={isRangeAgeButton ? null : value}
        thumbClassName="example-thumb"
        trackClassName="example-track"
        onChange={(val) => {
          setValue(val);
        }}
        renderThumb={(props, state) => <div {...props}>{state.valueNow}</div>}
        minDistance={12}
      />
    </div>
  );
}

export default SliderButton;
