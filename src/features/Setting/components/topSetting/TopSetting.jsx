import React from "react";
import LogoBox from "../logoBox/LogoBox";
import "./TopSetting.scss";

function TopSetting(props) {
  return (
    <div className="top-setting">
      <LogoBox title="A First Class Dating Experience" img="TPlatinum.svg" />
      <LogoBox title="See Who Likes You and More" img="TGold.svg" />
      <LogoBox title="Unlimited Like and More" img="TPlus.svg" />
    </div>
  );
}

export default TopSetting;
