import React from "react";
import { Link } from "react-router-dom";
import SettingButton from "../settingButton";
import "./style.scss";

function WebProfile(props) {
  return (
    <div>
      <Link to="/profile/webProfile">
        <SettingButton
          title="Username"
          currentValue="Claim Yours"
          icon={<i className="fas fa-angle-right" />}
        />
      </Link>
      <h6>
        Create a username. Share your name. Have people all over the world match
        with you tight on Tinder.
      </h6>
    </div>
  );
}

export default WebProfile;
