import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import SettingButton from "./components/settingButton";
import InnerSetting from "./pages/innerSetting/innerSetting";
import ListSetting from "./pages/listSetting/ListSetting";
import "./setting.scss";
import SwitchesButton from "./switchesButton";

function Setting(props) {
  return (
    <div className="setting">
      <Router>
        <Switch>
          <Route path="/profile/emailAccount">
            <InnerSetting>
              {/* <span>EMAIL</span> */}
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title=""
              />
            </InnerSetting>
          </Route>
          <Route path="/profile/phoneAccount">
            <InnerSetting>
              {/* <span>EMAIL</span> */}
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="0345678912"
              />
            </InnerSetting>
          </Route>
          <Route path="/profile/gender">
            <InnerSetting>
              {/* <span>EMAIL</span> */}
              <SettingButton
                isBorderBottom="true"
                // icon={<i className="fas fa-check" />}
                title="Man"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Woman"
              />
              <SettingButton
                isBorderBottom="true"
                // icon={<i className="fas fa-check" />}
                title="Everyone"
              />
            </InnerSetting>
          </Route>
          <Route path="/profile/languages">
            <InnerSetting>
              {/* <span>EMAIL</span> */}
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="English"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="English (Australia)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="English (United Kingdom)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Spanish"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Spanish (Argentina)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Spanish (Spain)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="French"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="French (Canada)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Japanese"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Korean"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Vietnamese"
              />
            </InnerSetting>
          </Route>
          <Route path="/profile/webProfile">
            <InnerSetting>
              <span>USERNAME</span>
              <SettingButton isBorderBottom="true" />
            </InnerSetting>
          </Route>
          <Route path="/profile/readReceipts">
            <InnerSetting>
              <SwitchesButton title="Send Read Receipts" />
              <h6>
                Turning this off will prevent any matches from activating read
                receipts in your conversation from this move forward.
              </h6>
            </InnerSetting>
          </Route>
          <Route path="/profile/recentlyActive">
            <InnerSetting>
              <SwitchesButton title="Recently Active Status" />
              <h6>
                Allow Teender members to see if you were recently active within
                the last 24h on Teender. If you have this turned off, they will
                not be able to see your recently active status.
              </h6>
            </InnerSetting>
          </Route>
          <Route path="/profile/email">
            <span>EMAIL</span>
            <SwitchesButton title="New Match" />
            <SwitchesButton title="Message" />
            <SwitchesButton title="Promotions">
              <h7>I want to receive news, update and offer from Teender</h7>
            </SwitchesButton>
            <h6>
              Control the emails you want to get - all of them, just the
              important stuff, or the bare minimum. you can always unsubscribe
              from the bottom of any email.
            </h6>
          </Route>

          <Route path="/profile/push">
            <span>EMAIL</span>
            <SwitchesButton title="New Matches" />
            <SwitchesButton title="Messages" />
            <SwitchesButton title="Message Likes">
              <h7>I want to receive news, update and offer from Teender</h7>
            </SwitchesButton>
            <SwitchesButton title="Super Likes">
              <h7>You have been Super Liked</h7>
            </SwitchesButton>
          </Route>
          <Route path="/profile/language">
            <InnerSetting>
              {/* <span>EMAIL</span> */}
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="English"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="English (Australia)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="English (United Kingdom)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Spanish"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Spanish (Argentina)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Spanish (Spain)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="French"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="French (Canada)"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Japanese"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Korean"
              />
              <SettingButton
                isBorderBottom="true"
                icon={<i className="fas fa-check" />}
                title="Vietnamese"
              />
            </InnerSetting>
          </Route>
          <Route path="/profile">
            <ListSetting />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default Setting;
