import React, { useEffect, useRef } from "react";
import "./innerSetting.scss";

function InnerSetting(props) {
  console.log(props.children);
  const listSettingRef = useRef();
  useEffect(() => {
    listSettingRef.current.style.transform = "translateX(0px)";
  }, []);
  return (
    <div
      ref={listSettingRef}
      style={{
        transition: "all .5s",
        transform: "translateX(1000px)",
      }}
      className="inner-setting"
    >
      {props.children}
    </div>
  );
}

export default InnerSetting;
