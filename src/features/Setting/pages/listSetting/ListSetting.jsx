import React, { useEffect, useRef } from "react";
import AccountSetting from "../../components/accountSetting";
import ActivityStatus from "../../components/activityStatus";
import Community from "../../components/community";
import DiscoverySetting from "../../components/discoverySetting/DiscoverySetting";
import HelpSupport from "../../components/helpSupport";
import Language from "../../components/language";
import Legal from "../../components/legal";
import Logout from "../../components/logout";
import Notification from "../../components/notification";
import ReadReceipts from "../../components/readReceipt";
import TopSetting from "../../components/topSetting/TopSetting";
import WebProfile from "../../components/webProfile";
import "./ListSetting.scss";

function ListSetting(props) {
  const listSettingRef = useRef();
  useEffect(() => {
    listSettingRef.current.style.transform = "translateX(0px)";
  }, []);
  return (
    <div
      ref={listSettingRef}
      style={{
        transform: "translateX(-1000px)",
        transition: "all .5s",
      }}
      className="list-setting"
    >
      <TopSetting />
      <h3>Account Setting</h3>
      <AccountSetting />
      <h3>Discovery Setting</h3>
      <DiscoverySetting />
      <h3>Web Profile</h3>
      <WebProfile />
      <h3>Read Receipts</h3>
      <ReadReceipts />
      <h3>Activity Status</h3>
      <ActivityStatus />
      <h3>Notification</h3>
      <Notification />
      <h3>Language</h3>
      <Language />
      <h3>Help & Support</h3>
      <HelpSupport />
      <h3>Community</h3>
      <Community />
      <h3>Legal</h3>
      <Legal />
      <Logout />
    </div>
  );
}

export default ListSetting;
