import React, { useState } from "react";
import Switch from "react-switch";
import "./style.scss";

function SwitchesButton(props) {
  const { title } = props;
  const [checked, setChecked] = useState(false);
  const handleChange = (nextChecked) => {
    setChecked(nextChecked);
  };

  return (
    <div className="switches-button">
      <div className="title">{title}</div>
      <Switch
        checked={checked}
        onChange={handleChange}
        onColor="#fd5068"
        onHandleColor="#ffffff"
        handleDiameter={30}
        uncheckedIcon={false}
        checkedIcon={false}
        boxShadow="0px 1px 5px rgba(0,0,0,0.6)"
        className="switches"
      />
    </div>
  );
}

export default SwitchesButton;
