import React from "react";
import "../createAccountForm.css";

export const Input = (props) => (
  <div className="container-input">
    <div className="input-wrapper">
      <label className="label-input" htmlFor="input-name">
        {props.label}
      </label>
      <div className="post-name">
        <input
          className="input-text"
          autoComplete="off"
          name={props.name}
          type={props.type}
          readOnly={props.readOnly}
          placeholder={props.placeholder}
          value={props.value}
          onChange={props.onChange}
          style={{
            width: props.width,
            "padding-left": props.paddingLeft,
          }}
        />
      </div>
      <span className="err-msg">{props.errMsg}</span>
    </div>
  </div>
);
