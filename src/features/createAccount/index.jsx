import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { GetGenders } from "../../actions/gender.action";
import { CreateMedia } from "../../actions/media.action";
import { AddSetting } from "../../actions/setting.action";
import { EditProfile, UpdateStatus } from "../../actions/user.action";
import { Input } from "./components/Input";
import "./createAccountForm.css";

function CreateAccount() {
  const setting = useSelector((state) => state.setting);
  const auth = useSelector((state) => state.auth);
  const media = useSelector((state) => state.media);
  const userEdit = useSelector((state) => state.userEdit);

  const [text, setText] = useState();
  const [checkText, setCheckText] = useState(false);
  const [textNameErr, setTextNameErr] = useState("");
  const [img, setImg] = useState("");
  const [age, setAge] = useState();
  const [checkAge, setCheckAge] = useState(false);
  const [ageErr, setAgeErr] = useState();
  const [gender, setGender] = useState();
  const [checkGender, setCheckGender] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();

  function hasNumber(myString) {
    return /\d/.test(myString);
  }

  const onSubmmit = () => {
    const editProfile = {
      firstName: text,
      gender,
      age,
    };
    const mediaInfo = {
      userId: auth.user._id,
      linkImg: img,
    };
    let genderId = "";
    if (gender === "6018ffe4f631b425c5ec57f4") {
      genderId = "60190010f631b425c5ec57f5";
      mediaInfo.img =
        "https://lh3.googleusercontent.com/-QpucJ7ceepw/XjgMmeOvkkI/AAAAAAACdsc/f6rHSExkOMgjFUMPlT7DYLHrocan7QH_ACNcBGAsYHQ/w500/97f28bea5cf33abff9d260848f5af9d7.png";
    }
    if (gender === "60190010f631b425c5ec57f5") {
      genderId = "6018ffe4f631b425c5ec57f4";
      mediaInfo.img =
        "https://scr.vn/wp-content/uploads/2020/07/%E1%BA%A2nh-%C4%91%E1%BA%A1i-di%E1%BB%87n-FB-m%E1%BA%B7c-%C4%91%E1%BB%8Bnh-n%E1%BB%AF.jpg";
    }
    dispatch(AddSetting(auth.user._id, genderId));
    dispatch(UpdateStatus(auth.user._id));
    dispatch(EditProfile(auth.user._id, editProfile));
    dispatch(CreateMedia(mediaInfo));
  };

  const handleChange = (e) => {
    if (e.target.name === "yourName") {
      setText(e.target.value);
      if (
        e.target.value.length < 1 ||
        e.target.value.length > 25 ||
        e.target.value === null ||
        e.target.value === ""
      ) {
        setTextNameErr("This cell must contain between 1 and 22 characters.");
        setCheckText(false);
      } else if (hasNumber(e.target.value)) {
        setCheckText(false);
        setTextNameErr("This cell cannot contain numbers.");
      } else {
        setTextNameErr("");
        setCheckText(true);
      }
    }

    if (e.target.name === "age") {
      setAge(e.target.value);
      if (Number.isNaN(e.target.value)) {
        setAgeErr("this cell must a number");
        setCheckAge(false);
      } else if (e.target.value === undefined || e.target.value === "") {
        setAgeErr("Age is not valid");
        setCheckAge(false);
      } else {
        setAgeErr("");
        setCheckAge(true);
      }
    }

    if (e.target.name === "gender") {
      setGender(e.target.value);
      if (e.target.value !== null) {
        setCheckGender(true);
      }
    }
  };

  if (setting.loading && media.loading && userEdit.loading) {
    history.push("/main");
  }

  useEffect(() => {
    dispatch(GetGenders());
  }, []);

  const genders = useSelector((state) => state.gender);

  return (
    <div>
      <div className="header">
        <div className="logo">
          <h1 className="text-logo">Teender</h1>
        </div>
        <div className="log-out">
          <span>Logout</span>
        </div>
      </div>
      <div className="container">
        <form className="create-account-form">
          <div className="title">
            <h1 className="text-title">Create account</h1>
          </div>
          <div className="input-element">
            <Input
              label="Name"
              placeholder="Enter your name"
              name="yourName"
              errMsg={textNameErr}
              value={text}
              onChange={(e) => handleChange(e)}
            />
            <div className="container-input">
              <div className="input-wrapper">
                <label className="label-input" htmlFor="input-name">
                  Your gender
                </label>
                <div className="post-name">
                  <select
                    className="input-text"
                    name="gender"
                    value={gender}
                    onChange={(e) => handleChange(e)}
                    style={{
                      width: "100px",
                      paddingLeft: "10px",
                    }}
                  >
                    <option selected disabled>
                      Gender
                    </option>
                    {genders.genderList.map((option) => (
                      <option key={option._id} value={option._id}>
                        {option.name}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="input-element">
            <Input label="Email" readOnly="readOnly" value={auth.user.email} />
            <Input
              label="Age"
              name="age"
              value={age}
              paddingLeft={10}
              width={100}
              errMsg={ageErr}
              onChange={(e) => handleChange(e)}
            />
          </div>
          <div className="input-element">
            <Input
              label="Your image"
              readOnly="readOnly"
              placeholder="upload image"
              type="file"
            />
            <div className="container-input" />
          </div>
          {checkText && checkAge && checkGender ? (
            <div className="input-element btn-continue" onClick={onSubmmit}>
              <button className="button-visible" type="submit">
                <span>continue</span>
              </button>
            </div>
          ) : (
            <div className="input-element btn-continue">
              <button className="button-disable" type="submit">
                <span>continue</span>
              </button>
            </div>
          )}
        </form>
      </div>
    </div>
  );
}

export default CreateAccount;
