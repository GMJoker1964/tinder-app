import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import NavMenu from "../features/NavMenu";
import CandidateList from "../features/CandidateList";
import Layout from "../HOC/Layout";
import locationApi from "../apis/locationApi";
import userCandidateApi from "../apis/userCandidateApi";
import action from "../actions";

function Main() {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  function showPosition(position) {
    if (!auth) return;
    locationApi.update({
      userId: auth.user._id,
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
    }).then((data) => {
      userCandidateApi.getCandidate(auth.user._id);
      // .then((data) => dispatch(action("ADD_USER", data)));
    }).catch((err) => {
    });
  }
  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    }
  }
  useEffect(() => {
    getLocation();
  }, []);

  return (
    <div>
      <Layout left={[NavMenu]} right={[CandidateList]} />
    </div>
  );
}

export default Main;
