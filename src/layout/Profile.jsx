import React from "react";
import Setting from "../features/Setting";
import Layout from "../HOC/Layout";
import CandidateList from "../features/CandidateList";

function Profile() {
  return (
    <div>
      <Layout left={[Setting]} right={[CandidateList]} />
    </div>
  );
}

export default Profile;
