import { authConstants } from "../constansts/index";

const initState = {
  token: null,
  user: JSON.parse(localStorage.getItem("user")) || {},
  authenticate: false,
  authenticating: false,
  loading: false,
  error: null,
  message: "",
};

export default (state = initState, action) => {
  switch (action.type) {
    case authConstants.LOGIN_REQUEST:
    { const newState = {
      ...state,
      authenticating: true,
    };
    return newState; }
    case authConstants.LOGIN_SUCCESS:
    { const newState = {
      ...state,
      user: action.payload.user,
      token: action.payload.token,
      authenticate: true,
      authenticating: false,
    };
    return newState;
    }
    case authConstants.LOGOUT_REQUEST:
    { const newState = {
      ...state,
      loading: true,
    };
    return newState; }
    case authConstants.LOGOUT_SUCCESS:
    { const newState = {
      ...initState,
    };
    return newState;
    }
    case authConstants.LOGOUT_FAILURE:
    { const newState = {
      ...state,
      error: action.payload.error,
      loading: false,
    };
    return newState;
    }
    default: return state;
  }
};
