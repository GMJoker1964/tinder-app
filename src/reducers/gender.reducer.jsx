import { getGenderConstants } from "../constansts/index";

const initState = {
  genderList: [],
  loading: false,
  error: null,
};

export default (state = initState, action) => {
  switch (action.type) {
    case getGenderConstants.GET_GENDERS_SUCCESS:
      state = {
        ...state,
        genderList: action.payload.genderList,
      };
      break;
    case getGenderConstants.GET_GENDERS_FAILURE:
      state = {
        ...state,
        error: action.payload.error,
      };
      break;
    default:
      break;
  }
  return state;
};
