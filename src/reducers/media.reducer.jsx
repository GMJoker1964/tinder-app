import { mediaConstants } from "../constansts/index";

const initState = {
  msg: "",
  loading: false,
};

export default (state = initState, action) => {
  switch (action.type) {
    case mediaConstants.CREATE_MEDIA_SUCCESS:
      state = {
        ...state,
        msg: "Add setting successfully",
        loading: true,
      };
      break;
    case mediaConstants.CREATE_MEDIA_FAILURE:
      state = {
        ...state,
        msg: "Add setting failure",
      };
      break;
    default:
      break;
  }
  return state;
};
