import { combineReducers } from "redux";
import authReducer from "./auth.reducer";
import genderReducer from "./gender.reducer";
import media from "./media.reducer";
import settingReducer from "./setting.reducer";
import candidate from "./candidateReucer";
import { default as userEdit, default as userReducer } from "./user.reducer";
import user from "./userReducer";

export default combineReducers({
  user,
  auth: authReducer,
  media,
  gender: genderReducer,
  setting: settingReducer,
  userEdit,
  userReudcer: userReducer,
  candidate,
});
