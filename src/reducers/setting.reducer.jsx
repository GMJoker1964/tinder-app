import { settingConstants } from "../constansts/index";

const initState = {
  msg: "",
  loading: false,
};

export default (state = initState, action) => {
  switch (action.type) {
    case settingConstants.ADD_SETTING_SUCCESS:
      state = {
        ...state,
        msg: "Add setting successfully",
        loading: true,
      };
      break;
    case settingConstants.ADD_SETTING_FAILURE:
      state = {
        ...state,
        msg: "Add setting failure",
      };
      break;
    default:
      break;
  }
  return state;
};
