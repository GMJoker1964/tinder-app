import { userConstants } from "../constansts/index";

const initState = {
  msg: "",
  loading: false,
};

export default (state = initState, action) => {
  switch (action.type) {
    case userConstants.UPDATE_PROFILE_SUCCESS:
      state = {
        ...state,
        msg: "update profile successfully",
        loading: true,
      };
      break;
    case userConstants.UPDATE_PROFILE_FAILURE:
      state = {
        ...state,
        msg: "Add profile failure",
      };
      break;
    default:
      break;
  }
  return state;
};
